<div class="layer-top background-top height-top">
    <div class="sub-top">
        <header class="website-developer">
            <p>DANILO CERNE - EXERCÍCIO</p>
        </header>
    </div>
</div>

<div class="layer-menu background-menu height-menu">
    <div class="sub-menu">
        <header>
            <h1>MENU</h1>
            <nav class="menu">
                <ul>
                    <li><a href="<?php echo $url_base."login"; ?>">LOGIN</a></li>
                    <li><a href="<?php echo $url_base."principal"; ?>">PRINCIPAL</a></li>
                    <li><a href="<?php echo $url_base."usuarios"; ?>">USUÁRIOS</a></li>
                </ul>
            </nav>
        </header>
    </div>
</div>