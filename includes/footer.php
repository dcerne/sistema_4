<div class="layer-footer">
    <div class="sub-footer">
        <footer>
            <section class="section-footer">
                <article class="layout-footer">
                    <p>Copyright © 2016 Danilo Cerne | Todos os direitos reservados.</p>
                    <p>Ribeirão Preto/SP.</p>
                </article>
            </section>
        </footer>
    </div>
</div>

<div class="layer-bottom background-bottom height-bottom">
    <div class="sub-bottom">
        <header class="website-developer float-right padding-bottom">
            <p>DANILO CERNE - EXERCÍCIO</p>
        </header>
    </div>
</div>