<?php
session_start();

try {
	
	$usuariosPermitidos = array("danilo" => "40bd001563085fc35165329ea1ff5c5ecbdbbeef", "leonardo" => "51eac6b471a284d3341d8c0c63d0f1a286262a18", "gabriel" => "fc1200c7a7aa52109d762a9f005b149abef01479", "fernando" => "8aefb06c426e07a0a671a1e2488b4858d694a730");
	$_SESSION['ARRAY_USUARIOS_PERMITIDOS_SESSION'] = $usuariosPermitidos;

	$username = addslashes(strip_tags(stripslashes(trim(strtolower($_POST["username"])))));
	$password = sha1(addslashes(strip_tags(stripslashes(trim(strtolower($_POST["password"]))))));

	//$_SESSION['USERNAME_SESSION'] = '';


	$loginOK = false;

	$count = 1;

	foreach ($usuariosPermitidos as $key => $value) {
		if (($username == $key) && ($password == $value)) {

			if ($_SESSION['LOGGED_USERS_SESSION'] < 5) {
			//if ($_COOKIE["LOGGED_USERS_SESSION"] < 5) {

				$_SESSION['USERNAME_SESSION'] = $username;
				$_SESSION['LOGGED_USERS_SESSION']++;

				if (!isset($_SESSION['LOGGED_USERS_COOKIE'])) {
					setcookie("LOGGED_USERS_COOKIE", $_SESSION['LOGGED_USERS_SESSION']);
				}

				
				//setcookie("LOGGED_USERS_COOKIE", $_SESSION['LOGGED_USERS_SESSION']);

				$loginOK = true;
				break;

			} else {
				$_SESSION['MSG_MAXIMUM_LOGGED_USERS_SESSION'] = "O número máximo de 5 usuários logados no sistema foi atingido!";
				$_SESSION['MSG_INVALID_LOGIN_DATA_SESSION'] = "";
				$_SESSION['USERNAME_SESSION'] = '';
				break;
			}

		} else {
			$_SESSION['MSG_INVALID_LOGIN_DATA_SESSION'] = "Usuário ou senha inválidos! Tente novamente!";
			//break;
		}
	}

	if ($loginOK) {
		header("Location: ../principal");
	} else {
		header("Location: ../login");
	}

} catch(Exception $e) {
	$_SESSION['MSG_AUTHENTICATION_ERROR_SESSION'] = $e->getTraceAsString();
}

?>