<?php
session_start();

include_once("includes/config/funcoes.php");

if (isset($_SESSION['USERNAME_SESSION']) && ($_SESSION['USERNAME_SESSION'] != "")) {
?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie lt-ie9 lt-ie8 lt-ie7" lang="pt-br"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie lt-ie9 lt-ie8" lang="pt-br"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie lt-ie9" lang="pt-br"> <![endif]-->
<html class="no-js" lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Usuários - Danilo Cerne - Exercício</title>
    <meta name="author" content="Danilo Cerne">    
    <meta name="url" content='' />
    <meta name="description" content="">
    <meta name="classification" content="">
    <meta name="format-detection" content="telephone=yes">
    <meta name="HandheldFriendly" content="true">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!-- <meta http-equiv="cleartype" content="on"> -->
    <link rel="icon" href="#" sizes="16x16">
    <!-- <meta http-equiv="cache-control"  content="particular, max-age=7200" /> -->

    
    <!-- [ STYLES: BOOTSTRAP.MIN.CSS ] -->
    <link rel="stylesheet" type="text/css" href="<?php echo url_base(); ?>/static/plugins/bootstrap/css/bootstrap.min.css">

    <!-- [ STYLES STYLE.CSS ] -->
    <link rel="stylesheet" type="text/css" href="<?php echo url_base(); ?>/static/css/paginas/style.css">
    

</head>
<body>

	<div class="wrapper">
        <?php require_once "includes/header.php"; ?>
        <div class="layer-content">
            <div class="sub-content">
                <!-- <div class="row"> -->
                    <main id="main" class="row">
                        <section class="section-content">
                            <article class="article-content">
                                <h1>USUÁRIOS</h1>
								<div class="panel panel-default">
									  <!-- Default panel contents -->
									  <!-- <div class="panel-heading">Dados de Acesso</div> -->

									  <!-- Table -->
									  <table class="table">
									  	<thead>
									  		<tr>
									  			<th>Username</th>
									  			<th>Password</th>
									  		</tr>
									  	</thead>
									  	<tbody>
										<?php
										if ((isset($_SESSION['ARRAY_USUARIOS_PERMITIDOS_SESSION'])) && (sizeof($_SESSION['ARRAY_USUARIOS_PERMITIDOS_SESSION']) > 0)) {

											foreach ($_SESSION['ARRAY_USUARIOS_PERMITIDOS_SESSION'] as $key => $value) {
										?>
												<tr style="text-align: left">
													<td><?php echo $key; ?></td>
													<td><?php echo "**************"; ?></td>
												</tr>
										<?php
											}
										}
										?>

										</tbody>
									  </table>  
								</div>
								<ul>
							        <li style="list-style: none;"><a href="javascript:window.history.go(-1)">Back</a></li>
							    	<li style="list-style: none;"><a href="<?php echo url_base()."/logout";?>">Logout</a></li>
								</ul>
                            </article>
                        </section>
                    </main>
                <!-- </div> -->
            </div>
        </div>

    </div>

	<?php require_once "includes/footer.php"; ?>


    <!-- [ JS JQUERY-1.12.2.MIN.JS ] -->
    <script src="<?php echo url_base(); ?>/static/plugins/bootstrap/js/jquery-1.12.2.min.js"></script> 
    <!-- [ JS BOOTSTRAP.MIN.JS ] -->
    <script src="<?php echo url_base(); ?>/static/plugins/bootstrap/js/bootstrap.min.js"></script> 

</body>
</html>

<?php
} else {
	header("Location: ../sistema_4/login");
}
?>