<?php
session_start();
//ob_start();
//setcookie("teste", 1);

include_once("includes/config/funcoes.php");


//MESSAGES
($_SESSION['MSG_AUTHENTICATION_ERROR_SESSION'] == "" ? $MSG_AUTHENTICATION_ERROR_SESSION = "" : $MSG_AUTHENTICATION_ERROR_SESSION = $_SESSION['MSG_AUTHENTICATION_ERROR_SESSION']);

($_SESSION['MSG_MAXIMUM_LOGGED_USERS_SESSION'] == "" ? $MSG_MAXIMUM_LOGGED_USERS_SESSION = "" : $MSG_MAXIMUM_LOGGED_USERS_SESSION = $_SESSION['MSG_MAXIMUM_LOGGED_USERS_SESSION']);

($_SESSION['MSG_INVALID_LOGIN_DATA_SESSION'] == "" ? $MSG_INVALID_LOGIN_DATA_SESSION = "" : $MSG_INVALID_LOGIN_DATA_SESSION = $_SESSION['MSG_INVALID_LOGIN_DATA_SESSION']);
//END MESSAGES

//SENHAS CRIPTOGRAFADAS
/*echo sha1("123");
echo "<br>";
echo sha1("456");
echo "<br>";
echo sha1("789");
echo "<br>";
echo sha1("000");
echo "<br>";*/

?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie lt-ie9 lt-ie8 lt-ie7" lang="pt-br"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie lt-ie9 lt-ie8" lang="pt-br"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie lt-ie9" lang="pt-br"> <![endif]-->
<html class="no-js" lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login - Danilo Cerne - Exercício</title>
    <meta name="author" content="Danilo Cerne">    
    <meta name="url" content='' />
    <meta name="description" content="">
    <meta name="classification" content="">
    <meta name="format-detection" content="telephone=yes">
    <meta name="HandheldFriendly" content="true">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!-- <meta http-equiv="cleartype" content="on"> -->
    <link rel="icon" href="#" sizes="16x16">
    <!-- <meta http-equiv="cache-control"  content="particular, max-age=7200" /> -->

    
    <!-- [ STYLES: BOOTSTRAP.MIN.CSS ] -->
    <link rel="stylesheet" type="text/css" href="<?php echo url_base(); ?>/static/plugins/bootstrap/css/bootstrap.min.css">

    <!-- [ STYLES STYLE.CSS ] -->
    <link rel="stylesheet" type="text/css" href="<?php echo url_base(); ?>/static/css/paginas/style.css">
    

</head>
<body>

	<div class="wrapper">
        <?php require_once "includes/header.php"; ?>
        <div class="layer-content">
            <div class="sub-content">
                <!-- <div class="row"> -->
                    <main id="main" class="row">
                        <section class="section-content">
                            <article class="article-content">
                                <h1>LOGIN</h1>
                                <p style="color: red"><?php echo $MSG_MAXIMUM_LOGGED_USERS_SESSION; ?></p>

								<p style="color: red"><?php echo $MSG_INVALID_LOGIN_DATA_SESSION; ?></p>

								<form action="actions/autenticacao.php" method="post">
									<label for="username">Username</label>
									<input type="text" name="username" />
									<label for="password">Password</label>
									<input type="password" name="password" />

									<button type="submit" >Entrar</button>
								</form>

								<a href="<?php echo url_base()."/reseta-usuarios-logados"; ?>" title="Resetar o número de usuários logados no sistema.">Resetar o número de usuários logados no sistema.</a>

								<div class="panel panel-default">
								  <!-- Default panel contents -->
								  <div class="panel-heading">Dados de Acesso</div>

								  <!-- Table -->
								  <table class="table">
								  	<thead>
								  		<tr>
								  			<th>Username</th>
								  			<th>Password</th>
								  		</tr>
								  	</thead>
								  	<tbody>
								  		<tr style="text-align: left">
								  			<td>Danilo</td>
								  			<td>123</td>
								  		</tr>
								  		<tr style="text-align: left">
								  			<td>Leonardo</td>
								  			<td>456</td>
								  		</tr>
								  		<tr style="text-align: left">
								  			<td>Gabriel</td>
								  			<td>789</td>
								  		</tr>
								  		<tr style="text-align: left">
								  			<td>Fernando</td>
								  			<td>000</td>
								  		</tr>
								  	</tbody>
								  </table>
								</div>
                            </article>
                        </section>
                    </main>
                <!-- </div> -->
            </div>
        </div>

    </div>

	<?php require_once "includes/footer.php"; ?>


    <!-- [ JS JQUERY-1.12.2.MIN.JS ] -->
    <script src="<?php echo url_base(); ?>/static/plugins/bootstrap/js/jquery-1.12.2.min.js"></script> 
    <!-- [ JS BOOTSTRAP.MIN.JS ] -->
    <script src="<?php echo url_base(); ?>/static/plugins/bootstrap/js/bootstrap.min.js"></script> 

</body>
</html>